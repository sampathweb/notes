Colorization - FastAI - https://www.facebook.com/FacebookforDevelopers/videos/340167420019712/

  

TF Keras Part 2 - https://www.youtube.com/watch?v=uhzGTijaw8A&feature=youtu.be&linkId=72781730

  

https://marcobonzanini.com/2015/01/05/my-python-code-is-slow-tips-for-profiling/

  

TF 2.0 - https://blog.paperspace.com/tensorflow-2-0-in-practice/

  

https://twitter.com/bremen79

https://blog.jonlu.ca/posts/tech-offers

  

find . -maxdepth 1 -name "*string*" -print

  
  

VPN

https://www.checkpoint.com/products/remote-access-vpn/

https://www.embraco.com/Restricted/VPN/

  

EX_LANDING and Leakage123

Internal IP is 10.13.21.3

  

Public DNS ec2-3-18-104-23.us-east-2.compute.amazonaws.com

User name Administrator

Password

?!6qsK3OTFqx4aJId*(f&yU?9GbF)Qo3

  

https://devtalk.nvidia.com/default/topic/1042234/performance-optimize-opencv-numpy-scipy/

  

https://developer.nvidia.com/how-to-cuda-python

  

https://courses.nvidia.com/courses/course-v1:DLI+C-RX-02+V1/about?ncid=so-twi-mn-95881

  

https://thenewstack.io/tutorial-configure-nvidia-jetson-nano-as-an-ai-testbed/

  

Install LLVM - Jetson:

https://devtalk.nvidia.com/default/topic/1051690/jetson-nano/instll-python-packages-librosa-and-llvm-on-jetson-nano-developer-kit-problem/

  
  

https://courses.nvidia.com/courses/course-v1:DLI+C-RX-02+V1/courseware/b2e02e999d9247eb8e33e893ca052206/26aa9f8bdaa948d9b068a8275c89e546/?child=first

https://courses.nvidia.com/courses/course-v1:DLI+C-RX-02+V1/courseware/b2e02e999d9247eb8e33e893ca052206/26aa9f8bdaa948d9b068a8275c89e546/?activate_block_id=block-v1%3ADLI%2BC-RX-02%2BV1%2Btype%40sequential%2Bblock%4026aa9f8bdaa948d9b068a8275c89e546

  

Concurrency:

  

https://realpython.com/python-concurrency/

https://medium.com/python-experiments/parallelising-in-python-mutithreading-and-mutiprocessing-with-practical-templates-c81d593c1c49

https://stackoverflow.com/questions/28056199/put-large-ndarrays-fast-to-multiprocessing-queue

  

HOG Features:

https://docs.opencv.org/master/d5/daf/tutorial_py_histogram_equalization.html

https://www.learnopencv.com/histogram-of-oriented-gradients/

  

Profiling:

https://github.com/jiffyclub/pycon-2018-talk

https://www.youtube.com/watch?v=yrRqNzJTBjk

  

https://www.youtube.com/watch?v=B9Kv3Fije1I

  
  
  

https://github.com/tensorflow/tensorflow/issues/29931

  

https://sumit-ghosh.com/articles/multiprocessing-vs-threading-python-data-science/

  
  

chunk_2676015DDFF1_1568743369 - Neighboring Compressor - OK

chunk_2676015DDFF1_1568678875 - Neighboring compressor Nozzle

chunk_2676015DDFF1_1568433225 - Close to ROI - Predicted NG

chunk_2676015DDFF1_1568750100 - Cannot miss this

  
  
  
  

https://www.pyimagesearch.com/2019/10/14/why-is-my-validation-loss-lower-than-my-training-loss/

  
  

https://colab.research.google.com/drive/1UCJt8EYjlzCs1H1d1X0iDGYJsHKwu-NO

https://twitter.com/fchollet/status/1180590885265756160

  
  
  

Today:

chunk_2676015DDFF1_1571086496.mp4

  
  

Last Processed:

chunk_2676015DDFF1_1570841305.mp4

  
  

How to build, publish and roll out a new Gru batch job definitions

Created by Pingyang He

Last updated Aug 15, 2019 by Tony Salim

  
  

Step 1: build new docker image and push to ECR

# login to ecr

aws ecr get-login --no-include-email --region us-east-2

  

# build container.

# in landing-zone/ml-infra-steps/aws-batch

docker build --tag=tf-1.13_v<your  version> --file=dockerfile-tf-1.13 .

  

# tag the image with aws

docker tag tf-1.13_v<your  version>:latest 286751717145.dkr.ecr.us-east-2.amazonaws.com/dl-train:tf-1.13_v<your  version>

  

# push the image:

docker push 286751717145.dkr.ecr.us-east-2.amazonaws.com/dl-train:tf-1.13_v<your  version>

Step 2: Check the new version is there ECR:

  

Go to https://us-east-2.console.aws.amazon.com/ecr/repositories/dl-train/?region=us-east-2

  

You should see something like this:

  

Step 3: Create a new job definition

  

In batch job definition page: https://us-east-2.console.aws.amazon.com/batch/home?region=us-east-2#/job-definitions , select the corresponding job def and clicks on "Create New Version". Use the newly pushed image there and save.

  

If you want to try it with Gru, when using lzone-cli command, you can specify `--job_def=<job name>`, for example: tf-1.13:4. If you want to make this the default job definition, follow the next step.

Step 4: Update the container mapping in Landing Zone Config DynamoDB

  

Go to https://us-east-2.console.aws.amazon.com/dynamodb/home?region=us-east-2#tables:selected=LandingZoneConfig;tab=items.

  

Choose config, update value → tf_version_to_job_def → 1.13:X with the new version X you just build and deployed.

On config, also ensure that value → default_tf_version is 1.1.3 (unless we change it in the future)

  
  

https://gru.landingai.io/job/12e4c8e0-8457-49eb-aef3-534de122fc90

  

https://gru.landingai.io/job/8d7e9970-7221-4a92-82d6-d8e45fee0dcf

  
  

Train104-100

Epoch 00100: saving model to ./output/run134/checkpoints/epoch_0100/cp.ckpt

  

Train114-100

  

https://github.com/tensorflow/tensorflow/tree/r1.13/tensorflow/tools/dockerfiles/dockerfiles

  
  

Custom CallbacK:

https://stackoverflow.com/questions/53736948/how-can-i-modify-modelcheckpoint-in-keras-to-monitor-both-val-acc-and-val-loss-a

https://stackoverflow.com/questions/45393429/keras-how-to-save-model-and-continue-training

https://github.com/keras-team/keras/blob/master/keras/callbacks/callbacks.py#L275

https://stackoverflow.com/questions/54175585/validation-accuracy-and-f1-score-remain-constant-right-from-first-epoch

  

https://www.tensorflow.org/guide/keras/custom_callback

https://keras.io/callbacks/#create-a-callback

  

https://github.com/tensorflow/tensorflow/releases

  

https://androidkt.com/precision-recall-and-f1/

  
  

https://medium.com/@vijayabhaskar96/multi-label-image-classification-tutorial-with-keras-imagedatagenerator-cd541f8eaf24

  
  

https://twitter.com/random_forests/status/1164226385726136320?s=20

  
  

https://container.training/ - Container Workshop

  

https://chrisachard.com/how-to-find-consulting-clients

  
  

Tensorboard Images:

https://colab.research.google.com/drive/1S9ThhinDflxU4KmTdxcv6_mE_vWwk2Jk#scrollTo=keKspwhadELy

https://www.tensorflow.org/tensorboard/image_summaries

https://stackoverflow.com/questions/43784921/how-to-display-custom-images-in-tensorboard-using-keras?rq=1

https://www.programcreek.com/python/example/104420/keras.callbacks.TensorBoard

https://keras.rstudio.com/articles/training_visualization.html

https://www.tensorflow.org/guide/data_performance

https://www.tensorflow.org/tutorials/load_data/images

https://lambdalabs.com/blog/tensorflow-2-0-tutorial-01-image-classification-basics/

  
  

Display images:

https://stackoverflow.com/questions/47877691/how-to-display-all-images-in-a-directory-with-flask

  
  

TF 2:

https://colab.research.google.com/drive/1TP3ESK7U1qhSV6zikZyS4VdBqESuJNXS#scrollTo=cakEiV4Gf5gK

  
  

Nice Video Collection of TF:

https://www.youtube.com/channel/UCHB9VepY6kYvZjj0Bgxnpbw/playlists

  
  
FastAPI:

  

https://pydantic-docs.helpmanual.io/

https://medium.com/analytics-vidhya/deploy-machine-learning-models-with-keras-fastapi-redis-and-docker-4940df614ece

  

https://fire.ci/blog/api-end-to-end-testing-with-docker/

  

https://danielsada.tech/blog/cloud-services-dos/

  
  

https://medium.com/octavian-ai/a-simple-explanation-of-the-inception-score-372dff6a8c7a

https://towardsdatascience.com/understanding-binary-cross-entropy-log-loss-a-visual-explanation-a3ac6025181a

  
  

Keras:

https://stackoverflow.com/questions/53355034/using-sample-weights-with-fit-generator

  
  

https://blog.alexellis.io/cooling-off-your-rpi4/

https://news.ycombinator.com/item?id=21539854
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTM3Mjg0MjkwMl19
-->